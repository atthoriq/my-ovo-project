# my-ovo-project

Apprenticeship Project

### v 1.0

Create Ansible Playbook that has 1 roles with 3 main tasks.
- Set Timezone to Asia/Jakarta
- Update all packages
- Set NTP Pool Indonesia

##### References
For the Set NTP Pool, i refer to the steps written in [this article](https://vitux.com/how-to-install-ntp-server-and-client-on-ubuntu/) as the tasks.

###### If you found any oversight in this project, I'd like to have a call to discuss it.